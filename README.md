This script changes the wallpaper on GTK based Desktop enviroments writing directly to gsettings.

To install:

Open wallpaper and set your Wallpaper directory.

Navigate to wallpaper_change folder and run ./install.

Enable by running systemctl --user enable wallpaper.timer.
Start by running systemctl --user start wallpaper.timer
